"use strict";
debugger;

function createNewUser() {
    const newUser = {
        _firstName: "",
        _lastName: "",
        _birthday: "",

        get firstName() {
            return this._firstName;
        },

        set firstName(newFirstName) {
            newFirstName = prompt("Input your Firstname:");

            while (newFirstName === "" || newFirstName == null) {
                alert("You have not entered Firstname!!");
                newFirstName = prompt("Input correct Firstname:");
            }
            this._firstName = newFirstName[0].toLowerCase();
        },

        get lastName() {
            return this._lastName;
        },

        set lastName(newLastName) {
            newLastName = prompt("Input your Lastname:");

            while (newLastName === "" || newLastName == null) {
                alert("You have not entered Lastname!!");
                newLastName = prompt("Input correct Lastname:");
            }
            this._lastName = newLastName.toLowerCase();
        },

        get birthday() {
            return this._birthday;
        },

        set birthday(newBirthday) {
            newBirthday = prompt("Input your Birthdate (dd.mm.yyyy):");

            const yearOfBirthDate = new Date((newBirthday.split('.').reverse().join(',')));
            this._yearOfBirth = yearOfBirthDate.getFullYear();

            const yearNow = new Date();
            this._currentYear = yearNow.getFullYear();

            this._birthday = this._currentYear - this._yearOfBirth;

            if (yearNow < new Date(yearOfBirthDate.setFullYear(this._currentYear))) {
                this._birthday = this._birthday - 1;
            }
            return this._birthday;
        },

        getLogin() {
            return (this._firstName + this._lastName);
        },

        getAge() {
            return this._birthday;
        },

        getPassword() {
            return (this._firstName + this._lastName + this._yearOfBirth);
        }
    };
    newUser.firstName = newUser._firstName;
    newUser.lastName = newUser._lastName;
    newUser.birthday = newUser._birthday;
    return newUser;
}
let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());